#include <iostream>
#include "Cost.h"

namespace rate {
	Cost::Cost() {
		m_buyer_name = "Petrov";
		m_people_number = 5;
		m_child_number = 3;
		m_cabin_class = 1;
		m_hotel_class = 4;
	}

	Cost::Cost(const char* buyer_name, int people_number, int child_number,
		int cabin_class, int hotel_class) {
		m_buyer_name = buyer_name;
		m_people_number = people_number;
		m_child_number = child_number;
		m_cabin_class = cabin_class;
		m_hotel_class = hotel_class;
	}

	Cost::~Cost() {
	
	}

	const char* Cost::GetBuyerName() {
		return m_buyer_name;
	}

	int Cost::GetPeopleNumber() {
		return m_people_number;
	}

	int Cost::GetChildNumber() {
		return m_child_number;
	}

	int Cost::GetCabinClass() {
		return m_cabin_class;
	}

	int Cost::GetHotelClass() {
		return m_hotel_class;
	}

	void Cost::SetBuyerName(const char* buyer_name) {
		m_buyer_name = buyer_name;
	}

	void Cost::SetPeopleNumber(int people_number) {
		m_people_number = people_number;
	}

	void Cost::SetChildNumber(int child_number) {
		m_child_number = child_number;
	}

	void Cost::SetCabinClass(int cabin_class) {
		m_cabin_class = cabin_class;
	}

	void Cost::SetHotelClass(int hotel_class) {
		m_hotel_class = hotel_class;
	}

	void Cost::Print() {
		system("cls");
		std::cout << m_buyer_name << std::endl;
		std::cout << m_people_number << std::endl;
		std::cout << m_child_number << std::endl;
		std::cout << m_cabin_class << std::endl;
		std::cout << m_hotel_class << std::endl;
	}

	int Cost::GetAirTicketCost() {
		switch (m_cabin_class) {

		case 1:
			return ((m_people_number-m_child_number)*200)+(m_child_number*100);
			break;
		case 2:
			return ((m_people_number - m_child_number) * 250) + (m_child_number * 125);
			break;
		case 3: 
			return ((m_people_number - m_child_number) * 300) + (m_child_number * 150);
			break;

		default:
			return 0;
			break;
		}
	}

	int Cost::GetHotelCost() {
		int FreeChild = 0 ;
		int AdultCount = 0;
		int result = 0;

		AdultCount = m_people_number - m_child_number;
		FreeChild = m_child_number - (AdultCount / 2);
		if (FreeChild < 0) { FreeChild = 0; }

		switch (m_hotel_class) {
		case 3:
				result = (AdultCount * 200 + FreeChild * 100);
			return result;
			break;
		case 4:
				result = (AdultCount * 280 + FreeChild * 140);
			return result;
			break;
		case 5:
			if (AdultCount >= 4) {
				result = (AdultCount * 400 + FreeChild * 200)-((AdultCount * 400 + FreeChild * 200)) / 10;
			}
			else
			{
				result = (AdultCount * 400 + FreeChild * 200);
			}
			return result;
			break;

		default:
			return 0;
			break;
		}
	}

	int Cost::GetDiscount() {
		int FreeChild = 0;
		int AdultCount = 0;

		AdultCount = m_people_number - m_child_number;
		FreeChild = m_child_number - (AdultCount / 2);
		if (FreeChild < 0) { FreeChild = 0; }

		if (m_hotel_class == 5 && AdultCount >= 4) {
			return (AdultCount * 400 + FreeChild * 200) / 10;
		}
		else
		{
			return 0;
		}
	}

}
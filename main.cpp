﻿#include <iostream>
#include "Cost.h"

using namespace rate;

void SetFamily(Cost& Fam);

int main()
{
	int FamilyCount = 0;
	int AllPeople = 0;
	int AllChild = 0;
	int AllDiscount = 0;
	int AllCost = 0;
	int FiveStarsCount = 0;

	setlocale(LC_ALL, "Russian");

	std::cout << "Введите кол-во семей:" << std::endl;
	std::cin >> FamilyCount;
	Cost* AllFamilys = new Cost[FamilyCount];

	for (int i = 0; i < FamilyCount; i++) {
	SetFamily(AllFamilys[i]);
	//AllFamilys[i].Print(); 
	std::cout <<std::endl<< "Цена путевки: ";
	std::cout << (AllFamilys[i].GetAirTicketCost()+AllFamilys[i].GetHotelCost())<<std::endl;
	std::cout << "Скидка: ";
	std::cout << AllFamilys[i].GetDiscount() << std::endl;
	std::cout << std::endl;

	AllPeople = AllPeople + AllFamilys[i].GetPeopleNumber();
	AllChild = AllChild + AllFamilys[i].GetChildNumber();
	AllDiscount = AllDiscount + AllFamilys[i].GetDiscount();
	AllCost = AllCost + AllFamilys[i].GetHotelCost() + AllFamilys[i].GetAirTicketCost();
	if (AllFamilys[i].GetHotelClass() == 5){ FiveStarsCount++; }
}
	std::cout << "Всего людей: " << AllPeople<< std::endl;
	std::cout << "Всего детей: " << AllChild << std::endl;
	std::cout << "Сумма всех скидок: " << AllDiscount << std::endl;
	std::cout << "Сумма всех путевок: " << AllCost << std::endl;
	std::cout << "Кол-во пятизвездночных путевок: " << FiveStarsCount << std::endl;
	delete[] AllFamilys;
}

void SetFamily(Cost& Fam){

	char* buffer = new char[128];
	int people_number = 0;
	int child_number = 0;
	int cabin_class = 0;
	int hotel_class = 0;

	std::cin.ignore();

	std::cout << "Введите имя покупателя: "<< std::endl;
	std::cin.getline(buffer,128);
	
	std::cout << "Введите число членов семьи: " << std::endl;
	std::cin >> people_number;

	std::cout << "Введите число детей: " << std::endl;
	std::cin >> child_number;

	std::cout << "Введите класс самолета: " << std::endl;
	std::cin >> cabin_class;

	std::cout << "Введите класс отеля: " << std::endl;
	std::cin >> hotel_class;

		Fam.SetBuyerName(buffer);
		Fam.SetPeopleNumber(people_number);
		Fam.SetChildNumber(child_number);
		Fam.SetCabinClass(cabin_class);
		Fam.SetHotelClass(hotel_class);
}
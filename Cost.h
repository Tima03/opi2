#pragma once

namespace rate {
	class Cost {
	public:

		Cost();
		Cost(const char* m_buyer_name, int m_people_number, int m_child_number,
			int m_cabin_class, int m_hotel_class);
		~Cost();

		const char* GetBuyerName();
		int GetPeopleNumber();
		int GetChildNumber();
		int GetCabinClass();
		int GetHotelClass();

		void SetBuyerName(const char* m_buyer_name);
		void SetPeopleNumber(int m_people_number);
		void SetChildNumber(int m_child_number);
		void SetCabinClass(int m_cabin_class);
		void SetHotelClass(int m_hotel_class);

		void Print();
		int GetAirTicketCost();
		int GetHotelCost();
		int GetDiscount();

	private:
		const char* m_buyer_name = nullptr;
		int m_people_number = 0;
		int m_child_number = 0;
		int m_cabin_class = 0;
		int m_hotel_class = 0;
		
	};
}